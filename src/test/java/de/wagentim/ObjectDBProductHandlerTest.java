package de.wagentim;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import javax.persistence.EntityManager;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.wagentim.constants.IObjcetDBConstants;
import de.wagentim.entity.Price;
import de.wagentim.entity.Product;
import de.wagentim.persistance.objectdb.ObjectDBProductHandler;


public class ObjectDBProductHandlerTest
{
	
	private ObjectDBProductHandler handler = null;
	String TEST_DB = IObjcetDBConstants.getTestDBPATH() + "test.odb";
	
	@Before
	public void init(){
		handler = new ObjectDBProductHandler(TEST_DB);
	}

	@Test
	public void testCreateEntityManager(){
		String uuid = handler.createEntityManager();
		assertTrue((uuid != null) && (!uuid.isEmpty()));
		assertNotNull(handler.getEntityManager(uuid));
	}

	private Product createNewProduct(){
		String productID = RandomStringUtils.randomNumeric(5);
		Product product = new Product();
		product.setProductID(Integer.parseInt(productID));
		return product;
	}
	
	@Test
	public void testSaveProduct2ProductMap()
	{
		Product p = createNewProduct();
		handler.addOrUpdateProduct(p, new Price(1.0, 0.0));

		assertTrue(handler.getAllProduct().size() == 1);
		assertTrue(handler.getAllProduct().contains(p));
	}

//		Pattern pattern = Pattern.compile("^https://[-a-zA-Z0-9+&@#%?=~_|!:,.;]*/[-a-zA-Z0-9+&@#%?=~_|!:,.;]*/([0-9]*)-");
/*		Matcher m = pattern.matcher(link);
		if(m.find())
		{
			System.out.println(m.group(0));
			System.out.println(m.group(1));
		}
	}
*/
	@After
	public void clear(){

		if(handler != null){
			handler.exit();
			handler = null;
		}

		File file = new File(TEST_DB);

		if(file.exists()){
			try {
				FileUtils.forceDelete(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
