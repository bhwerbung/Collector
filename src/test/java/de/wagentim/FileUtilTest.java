package de.wagentim;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import de.wagentim.constants.IFileConstants;
import de.wagentim.utils.FileUtil;

/**
 * Unit test for simple App.
 */
public class FileUtilTest 
{
    private FileUtil fu = null;

    @Before
    public void init(){
        fu = new FileUtil();
    }

    @Test
    public void shouldReadPropertiesFileFromResourceDir()
    {
        Properties properties = FileUtil.loadProperties(IFileConstants.FILE_SETTING);
        assertNotNull(properties);
    }
}
