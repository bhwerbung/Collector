package de.wagentim.entity;

import de.wagentim.constants.IStringConstants;

public class KeyValuePair
{
    private String key = IStringConstants.TXT_EMPTY_STRING;
    private String value = IStringConstants.TXT_EMPTY_STRING;

    public KeyValuePair()
    {
        this.key = IStringConstants.TXT_EMPTY_STRING;
        this.value = IStringConstants.TXT_EMPTY_STRING;
    }

    public KeyValuePair(String key, String value)
    {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isComplete()
    {
        return (!this.key.isEmpty()) && (!this.value.isEmpty());
    }
}
