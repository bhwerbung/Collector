package de.wagentim.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import de.wagentim.constants.IStringConstants;

@Getter @Setter @NoArgsConstructor @EqualsAndHashCode
public class Task {
    private String taskName = IStringConstants.EMPTY_STRING;
    private String clazzName = IStringConstants.EMPTY_STRING;
    private int crawler;
    private boolean headless;
    private boolean trace;

}
