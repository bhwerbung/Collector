package de.wagentim.entity;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class Setting {
    
    @Setter
    @Getter
    private boolean isHeadless = false;

    @Setter
    @Getter
    private List<String> sites;

    @Setter
    @Getter
    private int webDriverType = -1;
}
