package de.wagentim.constants;

import java.io.File;

import de.wagentim.utils.FileUtil;

public interface IObjcetDBConstants extends IDBConstants{

    public static final String DIR_DB_OBJECTDB = "objectdb";

    public static String getTestDBPATH(){
        
        return FileUtil.getCurrentPath() + File.separator + 
                    DIR_DB + File.separator + 
                    DIR_DB_OBJECTDB + File.separator + 
                    DIR_DB_TEST + File.separator;
    }

    public static String getProductDBPATH(){

        return FileUtil.getCurrentPath() + File.separator + 
                    DIR_DB + File.separator + 
                    DIR_DB_OBJECTDB + File.separator + 
                    DIR_DB_PRODUCT + File.separator;
    }

}
