package de.wagentim.constants;

public interface IStringConstants {

    public static final String EMPTY_STRING = "";
    public static final String TXT_NEW_LINE = "\n";
	public static final String TXT_EMPTY_STRING = "";
	public static final String TXT_SPACE = " ";
	public static final String TXT_DOLLAR = "$";
	public static final String TXT_COMMA = ",";
	public static final String TXT_DOUBLE_SPACE = "  ";
}
