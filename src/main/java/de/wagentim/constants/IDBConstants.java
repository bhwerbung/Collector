package de.wagentim.constants;

public interface IDBConstants {
    public static String DIR_DB = "db";
    public static String DIR_DB_TEST = "test";
    public static String DIR_DB_PRODUCT = "prod";
}
