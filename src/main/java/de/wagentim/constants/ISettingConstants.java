package de.wagentim.constants;

public interface ISettingConstants {
    
    public static final String SETTING_HEADLESS = "headless";
    public static final String SETTING_WEB_DRIVER_TYPE = "webdriver_type";
    public static final String SETTING_PROGRAM_MODE = "program_mode";
    public static final String SETTING_SITE = "sites";

    public static final int WEB_DRIVER_FIREFOX = 0;
    public static final int WEB_DRIVER_CHRMOE = 1;
    public static final int WEB_DRIVER_EDGE = 2;

}
