package de.wagentim.constants;

public interface IFileConstants {

    String FILE_SETTING = "setting.properties";
    String FILE_TASK = "task.json";
}
