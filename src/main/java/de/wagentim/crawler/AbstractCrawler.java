package de.wagentim.crawler;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.wagentim.entity.Setting;

public abstract class AbstractCrawler implements ICrawler{

    public static final String PROPERTY_FIREFOX = "webdriver.firefox.driver";
    public static final String PROPERTY_CHROME = "webdriver.chrome.driver";
    
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    
    protected WebDriver webDriver = null;
    protected Setting setting = null;

    public AbstractCrawler(Setting setting){
        this.setting = setting;
        initWebDriver();
    }

    protected List<WebElement> getWebElements(String link, String pattern) {
    
        if(StringUtils.isBlank(link)){
            logger.error("No valid input link: " + link);
            return null;
        }

        if(StringUtils.isBlank(pattern)){
            logger.error("No valid parser pattern: " + pattern);
            return null;
        }

        return webDriver.findElements(By.cssSelector(pattern));
    }

}
