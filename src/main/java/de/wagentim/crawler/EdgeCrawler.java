package de.wagentim.crawler;

// import com.paulhammant.ngwebdriver.NgWebDriver;

import de.wagentim.entity.Setting;
import de.wagentim.webdriversInitialer.EdgeWebDriverInitialer;

public class EdgeCrawler extends AbstractCrawler {

    public static final String PROPERTY_EDGE = "webdriver.edge.driver";
    public static final String DRIVER_NAME_WINDOWS_FIREFOX = "geckodriver.exe";
    
    public EdgeCrawler(Setting setting) {
        super(setting);
    }

    @Override
    public void initWebDriver() {
        
        webDriver = (new EdgeWebDriverInitialer(setting)).getWebDriver();

    }

    /*
    public WebElement getShadowRoot(WebElement shadowHost) {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;

        WebElement we = null;

        if (webDriver instanceof FirefoxDriver) {
            List<WebElement> results = (List<WebElement>) js.executeScript("return arguments[0].shadowRoot.children",
                    shadowHost);
            we = results.get(0);
        } else {
            we = (WebElement) js.executeScript("return arguments[0].shadowRoot", shadowHost);
        }
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(20));
        wait.until(ExpectedConditions.visibilityOfAllElements(we));
        return we;
    }

    public WebElement getWebElement(WebElement we, String selector, boolean shouldWait) {

        if (shouldWait) {
            WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(20));
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(selector)));
        }
        if (we == null) {
            return webDriver.findElement(By.cssSelector(selector));
        }

        return we.findElement(By.cssSelector(selector));
    }

    public List<WebElement> getWebElements(WebElement we, String selector, boolean shouldWait) {

        if (shouldWait) {
            WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(20));
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(selector)));
        }
        if (we == null) {
            return webDriver.findElements(By.cssSelector(selector));
        }

        return we.findElements(By.cssSelector(selector));
    }

    public void openPage(String link, boolean ngWait, boolean newTab) {
        if (link == null || link.isEmpty()) {
            logger.error("The input link is empty");
            return;
        }

        webDriver.get(link);

        if (ngWait) {
            // ngWebDriver.waitForAngularRequestsToFinish();
        }
    }

    public void printElementText(List<WebElement> list) {
        for (WebElement we : list) {
            System.out.println(we.getText());
        }
    }

    public void printElementHTML(List<WebElement> list) {
        for (WebElement we : list) {
            System.out.println(we.isDisplayed());
        }
    }

    protected void openTab(WebElement webElement, String link) {
        String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL, Keys.RETURN);
        webElement.sendKeys(selectLinkOpeninNewTab);
        ArrayList<String> tabs = new ArrayList<String>(webDriver.getWindowHandles());
        webDriver.switchTo().window(tabs.get(1));
        webDriver.get(link);
    }
*/
}
