package de.wagentim.utils;

public interface IGeneralConstant
{
	public static final String TXT_EMPTY_STRING = "";
	public static final String TXT_QUESTION_MARK = "?";
	public static final String TXT_UNDERLINE = "_";
	public static final String TXT_DOT = ".";
}
