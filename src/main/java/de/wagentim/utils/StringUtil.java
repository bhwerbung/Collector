package de.wagentim.utils;

import java.nio.charset.StandardCharsets;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

import de.wagentim.entity.KeyValuePair;
import de.wagentim.sites.ISite;

public class StringUtil
{
    public static final KeyValuePair parserKeyValue(String input, String deli)
    {
        StringTokenizer st = new StringTokenizer(input, deli);
        KeyValuePair kvp = new KeyValuePair();
        int index = 0;
        while(st.hasMoreElements())
        {
            String txt = st.nextToken().trim();
            if(index == 0)
            {
                kvp.setKey(txt);
            }
            else if(index == 1)
            {
                kvp.setValue(txt);
            }
            else
            {
                break;
            }

            index++;
        }

        return kvp;
    }

    public static String toUTF8(String input)
    {
        byte[] bytes = input.getBytes(StandardCharsets.UTF_8);
        return new String(bytes, StandardCharsets.UTF_8);
    }

    public static String getSitePackageName(){
        String fullName = ISite.class.getCanonicalName();
        return fullName.substring(0, fullName.indexOf(ISite.class.getSimpleName()));
    }
}
