package de.wagentim.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public final class FileUtil {

//	private final Logger logger = LoggerFactory.getLogger(FileUtil.class);

	public final static boolean fileExist(Path filePath) {
		LinkOption[] options = { LinkOption.NOFOLLOW_LINKS };

		return Files.exists(filePath, options);
	}
	
	public final static String getCurrentPath(){
		return Paths.get("").toAbsolutePath().toString();
	}

	public final static boolean createNewFile(Path filePath) {
		try {
			Path parent = filePath.getParent();

			if (!Files.exists(parent)) {
				Files.createDirectories(parent);
			}

			Files.createFile(filePath);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return true;
	}

	public final static String removeFileExtension(String filename, boolean removeAllExtensions) {
		if (filename == null || filename.isEmpty()) {
			return filename;
		}

		String extPattern = "(?<!^)[.]" + (removeAllExtensions ? ".*" : "[^.]*$");
		return filename.replaceAll(extPattern, "");
	}

    public final static Properties loadProperties(String fileName) {
     
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream resourceStream = loader.getResourceAsStream(fileName);
        Properties props = new Properties();
        try {
            props.load(resourceStream);
			return props;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

	public final static String readTextFile(String fileName) {

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		// Get the InputStream of the file from the classpath
		try (InputStream inputStream = classLoader.getResourceAsStream(fileName);
				InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
				BufferedReader reader = new BufferedReader(streamReader)) {

			StringBuilder content = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				content.append(line).append(System.lineSeparator());
			}
			return content.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
