package de.wagentim.sites;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.wagentim.crawler.ICrawler;

public abstract class AbstractSite implements ISite
{
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	protected ICrawler crawler = null;

	public void run(){
		execute();
	}

	protected abstract void execute();

	protected void print(String s)
	{
		logger.info(s);
	}

	public void setCrawler(ICrawler crawler){
		this.crawler = crawler;
	}
}