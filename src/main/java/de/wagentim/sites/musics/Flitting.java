package de.wagentim.sites.musics;

import de.wagentim.sites.AbstractSite;

public class Flitting extends AbstractSite
{


	@Override
	public void execute() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Unimplemented method 'execute'");
	}
/*
	private static final String SELECT_NET_MUSIC_RANKING_SONG_LIST = "#wlsong li";
	private static final String SELECT_MUSIC_NAME = "a.gname";
	private ObjectDBMusicHandler dbHandler;
	private String uuid = IConstants.TXT_EMPTY_STRING;
	private static final String SELECT_MUSIC_LINK = "div#musickrc audio#audio";
	private static int MAX_MUSICS = 15;
	
	private Logger logger = LoggerFactory.getLogger(Flitting.class);
	private FileDownloadUtil downloader;
	
	public Flitting()
	{
		webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		dbHandler = new ObjectDBMusicHandler("D:\\test\\flitting.odb");
		downloader = new FileDownloadUtil();
	}

	@Override
	public void execute()
	{
		uuid = dbHandler.createEntityManager();
		dbHandler.getAllMusicFromDB(uuid);
		
		// step 1: open first page
		openPage(getStartLink(), false, false);

		// step 2:
		List<WebElement> list = webDriver.findElements(By.cssSelector(SELECT_NET_MUSIC_RANKING_SONG_LIST));
		
		if(list == null || list.isEmpty())
		{
			logger.error("Cannot find Net Music Ranking Access Element");
			return;
		}

		Iterator<WebElement> it = list.iterator();

		int index = 0;

		List<Music> musics = new ArrayList<Music>();
		while(it.hasNext() && (index < MAX_MUSICS))
		{
			WebElement we = it.next();
			we = we.findElement(By.cssSelector(SELECT_MUSIC_NAME));
			String name = we.getText();
			KeyValuePair kvp = StringUtils.parserKeyValue(name, "-");
			Music m = new Music();
			m.setSinger(kvp.getKey());
			m.setSongName(kvp.getValue());

			if(!dbHandler.containMusic(m))
			{
				dbHandler.addMusic(m);
				String link = we.getAttribute("href");
				openTab(we, link);
				WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(5));
				//wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(SELECT_MUSIC_LINK)));
				wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SELECT_MUSIC_LINK)));
				String musicLink = webDriver.findElement(By.cssSelector(SELECT_MUSIC_LINK)).getAttribute("src");
				webDriver.close();
				ArrayList<String> tabs = new ArrayList<String>(webDriver.getWindowHandles());
				webDriver.switchTo().window(tabs.get(0));
				if(musicLink == null)
				{
					continue;
				}
				try {
					System.out.println(index);
					downloader.simpleDownloadFile(musicLink, MUSIC_SOURCE_DIR + m.getMusicNameMP3());
				} catch (IOException e) {
					e.printStackTrace();
				}
				index++;
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		//printMusics(musics);
		webDriver.quit();
		dbHandler.saveBackToDB(uuid);
		dbHandler.exit();
	}

	@Override
	protected String getStartLink()
	{
		return "http://flitting.cn/?l=1";
	}

	@Override
	protected String getSiteName()
	{
		return "Flitting";
	}
		*/

	@Override
	public String getStartLink() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Unimplemented method 'getStartLink'");
	}

	@Override
	public String getSiteName() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Unimplemented method 'getSiteName'");
	}
}
