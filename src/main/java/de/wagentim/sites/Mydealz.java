package de.wagentim.sites;

public class Mydealz extends AbstractSite{

    private String START_SITE = "https://www.mydealz.de/";
    private String SITE_NAME = "mydealz";

    @Override
    public void execute() {
        
    }

    @Override
    public String getStartLink() {
        return START_SITE;
    }

    @Override
    public String getSiteName() {
        return SITE_NAME;
    }

    
}