package de.wagentim;

import de.wagentim.controller.MainController;

public final class Starter {
    
    public static void main(String[] args) {
        new MainController().run();
    }
}
