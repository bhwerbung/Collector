package de.wagentim.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import de.wagentim.constants.ISettingConstants;
import de.wagentim.entity.Setting;

public final class SettingController {

//    private final Logger logger = LoggerFactory.getLogger(SettingController.class);

    public SettingController() {
    }

    public Setting parseSettings(Properties properties) {

        Setting setting = new Setting();

        // extract headless parameter
        setting.setHeadless(
                (Integer.parseInt(properties.getProperty(ISettingConstants.SETTING_HEADLESS)) == 0) ? true : false);


        // extract product sites
        String siteString = properties.getProperty(ISettingConstants.SETTING_SITE);
        List<String> allSites = Arrays.asList(StringUtils.splitPreserveAllTokens(siteString));
        setting.setSites(allSites);

        // extract web driver type (firefox, edge or chrome)
        setting.setWebDriverType(Integer.parseInt(properties.getProperty(ISettingConstants.SETTING_WEB_DRIVER_TYPE)));

        return setting;
    }
}
