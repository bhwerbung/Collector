package de.wagentim.controller;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.wagentim.constants.IFileConstants;
import de.wagentim.constants.ISettingConstants;
import de.wagentim.crawler.EdgeCrawler;
import de.wagentim.crawler.ICrawler;
import de.wagentim.entity.Setting;
import de.wagentim.sites.ISite;
import de.wagentim.utils.FileUtil;
import de.wagentim.utils.StringUtil;

public final class MainController {

    private final Logger logger = LoggerFactory.getLogger(MainController.class);
    private SettingController settingController;

    public MainController() {
        settingController = new SettingController();
    }

    public void run() {

        // Step 1: read the property file to read all settings
        Properties properties = FileUtil.loadProperties(IFileConstants.FILE_SETTING);
        logger.info("Read setting file: " + IFileConstants.FILE_SETTING);

        Setting setting = settingController.parseSettings(properties);

        // Step 2: decide which web browser will be used
        ICrawler crawler = null;
        int webDriverType = setting.getWebDriverType();

        switch (webDriverType) {
            case ISettingConstants.WEB_DRIVER_EDGE:
                crawler = new EdgeCrawler(setting);
        }

        if (crawler == null) {
            logger.error("Cannot create Crawler. Exit()");
            return;
        }

        // Step 3: proceed all sites
        List<String> allSites = setting.getSites();

        for (String s : allSites) {
            handlerSites(s, crawler);
        }
    }

    private void handlerSites(String className, ICrawler crawler) {
            
            try {
                Class<?> clazz = Class.forName(StringUtil.getSitePackageName() + StringUtils.capitalize(className));
                ISite site = (ISite) clazz.getDeclaredConstructor().newInstance();
                logger.debug("Proceed Site: " + site.getSiteName());
                site.setCrawler(crawler);
                site.run();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
    }

}
