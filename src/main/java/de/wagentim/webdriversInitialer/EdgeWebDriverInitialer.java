package de.wagentim.webdriversInitialer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.edge.EdgeOptions;

import de.wagentim.entity.Setting;

public class EdgeWebDriverInitialer extends AbstractWebDriverInitialer{

    public static final String PROPERTY_EDGE_DRIVER_KEY = "webdriver.edge.driver";
    public static final String DRIVER_NAME_WINDOWS_EDGE = "msedgedriver.exe";
    private EdgeOptions options;
    private EdgeDriverService service;

    public EdgeWebDriverInitialer(Setting setting) {
        super(setting);
    }

    @Override
    protected String getWindowsPropertyValue() {

        return getSystemPropertyValuePrefix() + DRIVER_NAME_WINDOWS_EDGE;
    }


    @Override
    protected String getWindowsPropertyKey() {
        return PROPERTY_EDGE_DRIVER_KEY;
    }

    @Override
    public WebDriver getWebDriver() {

        logger.info("Using Edge browser");
        WebDriver webDriver = new EdgeDriver(options);
        return webDriver;
    }

    @Override
    public void initialBrowserSpecialOptions(){
        logger.debug("Initial Edge special options start...");

        options = new EdgeOptions();

        if(setting.isHeadless()) {
            logger.debug("Set Edge browser headless");
            options.addArguments("--headless");
        }

        logger.debug("Initial Edge special options finished...");
    }

    @Override
    public void initialBrowserSpecialService(){
        logger.debug("Initial Edge special service start...");
        //service = new EdgeDriverService(null, 0, null, null, null);
        logger.debug("Initial Edge special service finished...");
    }
    
}
