package de.wagentim.webdriversInitialer;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.wagentim.entity.Setting;
import de.wagentim.utils.FileUtil;

public abstract class AbstractWebDriverInitialer implements IWebDriver{

    protected static final int INDEX_OS_WINDOWS = 0x00;
    protected static final int INDEX_OS_LINUX = 0x01;

    protected static final String OS_WINDOWS = "windows";
    protected static final String OS_LINUX = "linux";

    public static final String DRIVER_FOLDER = "drivers";

    protected Setting setting = null;

    protected final StringBuilder stringBuilder;

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    protected WebDriver webDriver = null;

    public AbstractWebDriverInitialer(Setting setting){
        this.setting = setting;
        stringBuilder = new StringBuilder();
        init();
    }
    
    
    private void init() {
        setSystemProperty();
        initWebDriver();
    }


    private int getOperationSystem(){
        
        String os = System.getProperty("os.name").toLowerCase();
        
        if(os.contains(OS_WINDOWS)){
            return INDEX_OS_WINDOWS;
        }
        else if(os.contains(OS_LINUX)){
            return INDEX_OS_LINUX;
        }
        else{
            throw new RuntimeException("Cannot identify the current operation system: " + os);
        }
    }
    
    private void setSystemProperty(){

        int os = getOperationSystem();

        switch(os){
            case INDEX_OS_WINDOWS:
                logger.info("Windows System");
                logger.debug("Set system property (" + getWindowsPropertyKey() + ", " + getWindowsPropertyValue() + ")");
                System.setProperty(getWindowsPropertyKey(), getWindowsPropertyValue());
                break;
        }
    }

    protected String getSystemPropertyValuePrefix(){
        
        clearStringBuilder();

        stringBuilder.append(FileUtil.getCurrentPath());
        stringBuilder.append(File.separator);
        stringBuilder.append(DRIVER_FOLDER);
        stringBuilder.append(File.separator);

        switch(getOperationSystem()){

            case INDEX_OS_WINDOWS:
                stringBuilder.append(OS_WINDOWS);
                break;

            case INDEX_OS_LINUX:
                stringBuilder.append(OS_LINUX);
                break;

            default:
                throw new RuntimeException("Cannot identify the current operation system: " + System.getProperty("os.name").toLowerCase());
        }

        stringBuilder.append(File.separator);

        return stringBuilder.toString();
    }

    protected void clearStringBuilder(){
        stringBuilder.delete(0, stringBuilder.length());
    }

    protected abstract String getWindowsPropertyValue();

    protected abstract String getWindowsPropertyKey();

    public abstract WebDriver getWebDriver();

    protected void initWebDriver(){
        
        initialBrowserSpecialService();
        initialBrowserSpecialOptions();
    }

    public abstract void initialBrowserSpecialOptions();

    public abstract void initialBrowserSpecialService();
}
